import { Book } from '../interfaces/book.interface';

export const mockBook: Book = {
  bookTitle: 'Title',
  authors: ['Author1, Author2'],
  firstPublicationYear: 1950,
  coverImage: '',
  coverThumbnail: '',
};
