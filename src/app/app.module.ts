import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './components/book/book.component';
import { FloatInputComponent } from './components/float-input/float-input.component';
import { ButtonComponent } from './components/button/button.component';
import { ModalComponent } from './components/modal/modal.component';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { DeferLoadDirective } from './directives/defer-load.directive';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    FloatInputComponent,
    ButtonComponent,
    DeferLoadDirective,
    LoadingIndicatorComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent],
})
export class AppModule {}
