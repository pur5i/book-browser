import { Component, OnInit, Input } from '@angular/core';

export enum ButtonType {
  BUTTON = 'button',
  SUBMIT = 'submit',
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input() type: ButtonType;

  constructor() {}

  ngOnInit(): void {}
}
