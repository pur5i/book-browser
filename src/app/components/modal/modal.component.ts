import { Component } from '@angular/core';

import { Subject } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  onClose = new Subject();

  constructor() {}

  onCloseModal(): void {
    this.onClose.next();
  }
}
