import { Component, Input } from '@angular/core';

export enum LoaderSize {
  DEFAULT,
  SMALL,
}

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss'],
})
export class LoadingIndicatorComponent {
  readonly LoaderSize = LoaderSize;

  @Input() loaderSize = LoaderSize.DEFAULT;

  constructor() {}
}
