import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  ControlContainer,
  FormGroupDirective,
  FormControl,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-float-input',
  templateUrl: './float-input.component.html',
  styleUrls: ['./float-input.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ],
})
export class FloatInputComponent implements OnInit, OnDestroy {
  inputHasValue = false;
  ngDestroyed$ = new Subject();

  @Input() controlName: string;
  @Input() inputLabel: string;
  @Input() control: FormControl;

  constructor() {}

  ngOnInit(): void {
    this.control?.valueChanges
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((val) => {
        this.inputHasValue = val.length > 0;
      });
  }

  ngOnDestroy(): void {
    this.ngDestroyed$.next();
  }

  get input() {
    return this.control;
  }
}
