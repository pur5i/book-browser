import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatInputComponent } from './float-input.component';
import { FormGroupDirective, FormControl } from '@angular/forms';

describe('FloatInputComponent', () => {
  let component: FloatInputComponent;
  let fixture: ComponentFixture<FloatInputComponent>;
  const formControl = new FormControl('test');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FloatInputComponent],
      providers: [FormGroupDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatInputComponent);
    component = fixture.componentInstance;
    component.control = formControl;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
