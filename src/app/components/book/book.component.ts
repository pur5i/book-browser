import {
  Component,
  OnInit,
  Input,
  ViewChild,
  TemplateRef,
} from '@angular/core';

import { Book } from 'src/app/interfaces/book.interface';
import { LoaderSize } from '../loading-indicator/loading-indicator.component';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent implements OnInit {
  readonly LoaderSize = LoaderSize;

  showThumbnail = false;

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  @Input() book: Book;

  constructor(private modalService: ModalService) {}

  ngOnInit(): void {}

  revealThumbnail() {
    this.showThumbnail = true;
  }

  onOpenModal() {
    this.modalService.open(this.modalContent);
  }
}
