import {
  Directive,
  ElementRef,
  EventEmitter,
  Output,
  AfterViewInit,
  Input,
} from '@angular/core';

@Directive({
  selector: '[appDeferLoad]',
})
export class DeferLoadDirective implements AfterViewInit {
  @Output() deferLoad = new EventEmitter();
  @Input() triggerOutsideView = false;

  private intersectionObserver: IntersectionObserver;

  constructor(private element: ElementRef) {}

  ngAfterViewInit() {
    this.intersectionObserver = new IntersectionObserver((entries) => {
      this.checkForIntersection(entries);
    }, {});
    this.intersectionObserver.observe(this.element.nativeElement);
  }

  private checkForIntersection(entries: IntersectionObserverEntry[]) {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (
        entry.target === this.element.nativeElement &&
        this.triggerOutsideView
      ) {
        this.deferLoad.emit(entry.intersectionRatio === 0);
      }
      if (this.checkIfIntersecting(entry) && !this.triggerOutsideView) {
        this.deferLoad.emit();
        this.intersectionObserver.unobserve(this.element.nativeElement);
        this.intersectionObserver.disconnect();
      }
    });
  }

  private checkIfIntersecting(entry: IntersectionObserverEntry) {
    return entry.isIntersecting && entry.target === this.element.nativeElement;
  }
}
