export interface Book {
  bookTitle: string;
  authors: string[];
  firstPublicationYear: number;
  coverImage: string;
  coverThumbnail: string;
}
