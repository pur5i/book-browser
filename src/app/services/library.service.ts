import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Book } from '../interfaces/book.interface';

enum CoverImageSize {
  LARGE = 'L',
  SMALL = 'S',
}

@Injectable({
  providedIn: 'root',
})
export class LibraryService {
  constructor(private http: HttpClient) {}

  searchTitle(title: string): Observable<Book[]> {
    return this.http
      .get(`http://openlibrary.org/search.json?title=${title}`)
      .pipe(
        map((res: any): Book[] =>
          res.docs.map((doc: any): Book => this.mapBookData(doc))
        ),
        catchError((error) => throwError(error))
      );
  }

  private mapBookData({
    title,
    author_name,
    cover_i,
    first_publish_year,
  }): Book {
    return {
      bookTitle: title,
      authors: author_name,
      firstPublicationYear: first_publish_year,
      coverImage: this.parseImageSrc(cover_i, CoverImageSize.LARGE),
      coverThumbnail: this.parseImageSrc(cover_i, CoverImageSize.SMALL),
    };
  }

  private parseImageSrc(id: number, size: CoverImageSize): string {
    return id && id !== -1
      ? `http://covers.openlibrary.org/b/id/${id}-${size}.jpg`
      : undefined;
  }
}
