import {
  Injectable,
  ComponentRef,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  Inject,
  TemplateRef,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { ModalComponent } from '../components/modal/modal.component';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  modalComponentRef: ComponentRef<ModalComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    @Inject(DOCUMENT) private document: Document
  ) {}

  open(template: TemplateRef<any>) {
    this.appendModal(template);
    this.modalComponentRef.instance.onClose.subscribe(() => {
      this.remove();
    });
  }

  private remove() {
    this.document.body.classList.remove('modal-open');
    this.appRef.detachView(this.modalComponentRef.hostView);
    this.modalComponentRef.destroy();
  }

  private appendModal(template: TemplateRef<any>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      ModalComponent
    );
    const ngTemplate = this.resolveNgTemplate(template);
    const componentRef = componentFactory.create(this.injector, ngTemplate);
    const { nativeElement } = componentRef.location;

    this.document.body.appendChild(nativeElement);
    this.document.body.classList.add('modal-open');
    this.modalComponentRef = componentRef;
  }

  private resolveNgTemplate(template: TemplateRef<any>) {
    const viewRef = template.createEmbeddedView(null);
    this.appRef.attachView(viewRef);
    return [viewRef.rootNodes];
  }
}
