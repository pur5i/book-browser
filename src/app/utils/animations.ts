import { animation, animate, style } from '@angular/animations';

const easeOut = 'cubic-bezier(.17,.67,.36,1)';

export const fadeIn = animation([
  style({ opacity: 0 }),
  animate(`300ms ${easeOut}`, style({ opacity: 1 })),
]);

export const fadeOut = animation([
  animate(`300ms ${easeOut}`, style({ opacity: 0 })),
]);

export const revealFromBottom = animation([
  style({ opacity: 0, transform: 'translateY(10%)' }),
  animate(
    `300ms ${easeOut}`,
    style({ opacity: 1, transform: 'translateY(0)' })
  ),
]);

export const concealToBottom = animation([
  style({ opacity: 1, transform: 'translateY(0)' }),
  animate(
    `300ms ${easeOut}`,
    style({ opacity: 0, transform: 'translateY(10%)' })
  ),
]);
