import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { trigger, transition } from '@angular/animations';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { LibraryService } from './services/library.service';
import { ButtonType } from './components/button/button.component';
import { Book } from './interfaces/book.interface';
import {
  fadeIn,
  fadeOut,
  revealFromBottom,
  concealToBottom,
} from './utils/animations';

enum SearchStatus {
  INITIAL,
  LOADING,
  COMPLETE,
  ERROR,
}

enum SearchFormField {
  SEARCH = 'search',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', fadeIn),
      transition(':leave', fadeOut),
    ]),
    trigger('revealIn', [
      transition(':enter', revealFromBottom),
      transition(':leave', concealToBottom),
    ]),
  ],
})
export class AppComponent implements OnInit {
  readonly ButtonType = ButtonType;
  readonly SearchStatus = SearchStatus;
  readonly SearchFormField = SearchFormField;

  @ViewChild('back') back: ElementRef;

  reactiveForm: FormGroup;
  books$: Observable<Book[]>;
  status = SearchStatus.INITIAL;

  constructor(
    private fb: FormBuilder,
    private libService: LibraryService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  onSubmit(): void {
    if (this.reactiveForm.valid) {
      this.status = SearchStatus.LOADING;
      this.books$ = this.libService
        .searchTitle(this.reactiveForm.value.search)
        .pipe(
          map((books) => {
            this.status = SearchStatus.COMPLETE;
            return books;
          }),
          catchError(() => {
            this.status = SearchStatus.ERROR;
            return of([]);
          })
        );
    } else {
      this.reactiveForm.markAllAsTouched();
    }
  }

  onHandleScrollBtn(state: boolean) {
    this.back.nativeElement.classList.toggle('back--visible', state);
  }

  scrollToTop() {
    this.document.body.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  private createForm(): void {
    this.reactiveForm = this.fb.group({
      [SearchFormField.SEARCH]: ['', Validators.required],
    });
  }
}
